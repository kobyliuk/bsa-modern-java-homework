package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

/**
 * AttackSubsystemImpl class.
 *
 * @author Andrii Kobyliuk
 */
public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger powergridRequirments;

	private final PositiveInteger getCapacitorConsumption;

	private final PositiveInteger optimalSpeed;

	private final PositiveInteger optimalSize;

	private final PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger powergridRequirments,
			PositiveInteger getCapacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) {
		this.name = name;
		this.powergridRequirments = powergridRequirments;
		this.getCapacitorConsumption = getCapacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.getCapacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {

		double sizeReductionModifier = getSizeReductionModifier(target.getSize());
		double speedReductionModifier = getSpeedReductionModifier(target.getCurrentSpeed());

		return PositiveInteger
				.of((int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
	}

	@Override
	public String getName() {
		return this.name;
	}

	private double getSizeReductionModifier(PositiveInteger targetSize) {
		if (targetSize.value() >= this.optimalSize.value()) {
			return 1;
		}
		else {
			return targetSize.value() / (double) this.optimalSize.value();
		}
	}

	private double getSpeedReductionModifier(PositiveInteger targetSpeed) {
		if (targetSpeed.value() <= this.optimalSpeed.value()) {
			return 1;
		}
		else {
			return this.optimalSpeed.value() / (double) (2 * targetSpeed.value());
		}
	}

}
