package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.common.impl.NamedEntityImpl;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

/**
 * CombatReadyShip class.
 *
 * @author Andrii Kobyliuk
 */
public final class CombatReadyShip implements CombatReadyVessel {

	private final DockedShip ship;

	private int currentCapacityAmount;

	private boolean canAttack = false;

	private int shieldDamageToRepair = 0;

	private int hullDamageToRepair = 0;

	public CombatReadyShip(DockedShip ship) {
		this.ship = ship;
		this.currentCapacityAmount = ship.getCapacitorAmount();
	}

	@Override
	public void endTurn() {

		int totalCapacityAmount = this.ship.getCapacitorAmount();
		int rechargeValue = this.ship.getCapacitorRechargeRate();

		// recharge capacitor in certain amount of energy
		this.currentCapacityAmount = Math.min(this.currentCapacityAmount + rechargeValue, totalCapacityAmount);
	}

	@Override
	public void startTurn() {

		// each new turn ship can attack only once
		this.canAttack = true;
	}

	@Override
	public String getName() {
		return this.ship.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {

		// cannot attack twice by one turn
		if (!this.canAttack) {
			return Optional.empty();
		}

		AttackSubsystem attackSubsystem = this.ship.getAttackSubsystem();
		int attackPowerConsumption = attackSubsystem.getCapacitorConsumption().value();

		// need enough energy to attack
		if (this.currentCapacityAmount >= attackPowerConsumption) {
			int damage = attackSubsystem.attack(target).value();
			NamedEntity attackerName = new NamedEntityImpl(this.ship.getName());
			NamedEntity targetName = new NamedEntityImpl(target.getName());
			NamedEntity weaponName = new NamedEntityImpl(attackSubsystem.getName());

			// reduce energy after attack
			this.currentCapacityAmount -= attackPowerConsumption;

			// ban on attacking in current turn
			this.canAttack = false;

			return Optional.of(new AttackAction(PositiveInteger.of(damage), attackerName, targetName, weaponName));
		}

		return Optional.empty();
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {

		DefenciveSubsystem defenciveSubsystem = this.ship.getDefenciveSubsystem();
		int shieldHPAvailable = this.ship.getShieldHP() - this.shieldDamageToRepair;
		int hullHPAvailable = this.ship.getHullHP().value() - this.hullDamageToRepair;

		AttackAction attackImpact = defenciveSubsystem.reduceDamage(attack);
		int damageTaken = attackImpact.damage.value();

		// damage over available total HP (shield + hull)
		if (damageTaken >= shieldHPAvailable + hullHPAvailable) {
			return new AttackResult.Destroyed();
		}

		// shield HP reducing
		int differenceTmp = this.shieldDamageToRepair;
		this.shieldDamageToRepair = Math.min(this.ship.getShieldHP(), damageTaken + this.shieldDamageToRepair);
		differenceTmp -= this.shieldDamageToRepair;

		// hull HP reducing
		damageTaken += differenceTmp;
		this.hullDamageToRepair = Math.min(this.ship.getHullHP().value(), damageTaken + this.hullDamageToRepair);

		return new AttackResult.DamageRecived(attackImpact.weapon, attackImpact.damage, attackImpact.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {

		DefenciveSubsystem defenciveSubsystem = this.ship.getDefenciveSubsystem();
		int defencivePowerConsumption = defenciveSubsystem.getCapacitorConsumption().value();

		// less energy available than needed
		if (this.currentCapacityAmount < defencivePowerConsumption) {
			return Optional.empty();
		}

		// current capacity always reduced when enough energy available
		this.currentCapacityAmount -= defencivePowerConsumption;

		// full shield and hp case
		if (this.shieldDamageToRepair == 0 && this.hullDamageToRepair == 0) {
			return Optional.of(new RegenerateAction(PositiveInteger.of(0), PositiveInteger.of(0)));
		}

		// Regeneration actions
		RegenerateAction regenerateAction = defenciveSubsystem.regenerate();
		int actuallyShieldHPRegenerated = 0;
		int actuallyHullHPRegenerated = 0;

		// shield regeneration case
		if (this.shieldDamageToRepair != 0) {
			int shieldRegenerationAmount = regenerateAction.shieldHPRegenerated.value();
			actuallyShieldHPRegenerated = Math.min(this.shieldDamageToRepair, shieldRegenerationAmount);
			this.shieldDamageToRepair -= actuallyShieldHPRegenerated;
		}

		// hull regeneration case
		if (this.hullDamageToRepair != 0) {
			int hullRegeneration = regenerateAction.hullHPRegenerated.value();
			actuallyHullHPRegenerated = Math.min(this.hullDamageToRepair, hullRegeneration);
			this.hullDamageToRepair -= actuallyHullHPRegenerated;
		}

		return Optional.of(new RegenerateAction(PositiveInteger.of(actuallyShieldHPRegenerated),
				PositiveInteger.of(actuallyHullHPRegenerated)));
	}

}
