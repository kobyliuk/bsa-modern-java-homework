package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

/**
 * DockedShip class.
 *
 * @author Andrii Kobyliuk
 */
public final class DockedShip implements ModularVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private PositiveInteger powerGridOutput;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powerGridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powerGridOutput = powerGridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		return new DockedShip(name, shieldHP, hullHP, powerGridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {

		// always sets subsystem even if it equals null
		this.attackSubsystem = subsystem;

		if (subsystem == null) {
			return;
		}

		int attackSystemConsumption = subsystem.getPowerGridConsumption().value();
		allocatePowerConsumption(attackSystemConsumption);
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {

		// always sets subsystem even if it equals null
		this.defenciveSubsystem = subsystem;

		if (subsystem == null) {
			return;
		}

		int defensiveSystemConsumption = subsystem.getPowerGridConsumption().value();
		allocatePowerConsumption(defensiveSystemConsumption);
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {

		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}

		if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}

		if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return new CombatReadyShip(this);
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public String getName() {
		return this.name;
	}

	public int getShieldHP() {
		return this.shieldHP.value();
	}

	public int getCapacitorAmount() {
		return this.capacitorAmount.value();
	}

	public int getCapacitorRechargeRate() {
		return this.capacitorRechargeRate.value();
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	private void allocatePowerConsumption(int powerConsumption) throws InsufficientPowergridException {

		int energyAvailable = this.powerGridOutput.value();
		int difference = energyAvailable - powerConsumption;

		if (difference < 0) {
			throw new InsufficientPowergridException(-difference);
		}
		else {
			this.powerGridOutput = PositiveInteger.of(difference);
		}
	}

}
