package com.binary_studio.dependency_detector;

/**
 * Dependency Detector class.
 *
 * @author Andrii Kobyliuk
 */
public final class DependencyDetector {

	private DependencyDetector() {
	}

	/**
	 * Solution method
	 * @param libraries - the dependencies list
	 * @return returns true - if the dependencies are absent else returns false
	 */
	public static boolean canBuild(DependencyList libraries) {

		// approach doesn't work if libs have the same naming
		int nLibs = libraries.libraries.size();

		// marks that library has already depended on other libraries
		boolean[] isAlreadyDependsOn = new boolean[nLibs];

		for (String[] dependencyLink : libraries.dependencies) {
			int libIndex = libraries.libraries.indexOf(dependencyLink[0]);
			int dependencyIndex = libraries.libraries.indexOf(dependencyLink[1]);

			// if the dependency`s flag 'depends on' is false than we can chain +1 library
			if (!isAlreadyDependsOn[dependencyIndex]) {
				isAlreadyDependsOn[libIndex] = true;
			}
			else {
				return false;
			}
		}

		return true;
	}

}
