package com.binary_studio.academy_coin;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * Academy Coin class.
 *
 * A simple approach is to try buying the academy coins and selling them on every single
 * day when profitable and keep updating the maximum profit so far.
 *
 * @author Andrii Kobyliuk
 */
public final class AcademyCoin {

	private AcademyCoin() {
	}

	/**
	 * Solution method
	 * @param prices - the stream of stocks
	 * @return returns maximum profit for the given stream of numbers
	 */
	public static int maxProfit(Stream<Integer> prices) {

		int[] pricesArray = prices.mapToInt(i -> i).toArray();

		return getProfit(pricesArray, 0, pricesArray.length - 1);
	}

	/**
	 * Helper method for recursion usage
	 * @param price - array of stocks
	 * @param start - start bound of search
	 * @param end - end bound of search
	 * @return maximum profit for the given array and bounds
	 */
	private static int getProfit(int[] price, int start, int end) {

		// cant bought anything
		if (end <= start) {
			return 0;
		}

		// just a wrapper for updating inside lambda
		AtomicInteger profit = new AtomicInteger();

		// O(n^2) loop for searching the result
		Stream.iterate(start, i -> i < end, i -> i + 1)
				.forEach(i -> Stream.iterate(i + 1, j -> j <= end, j -> j + 1).forEach(j -> {
					if (price[j] > price[i]) {
						int currentProfit = price[j] - price[i] + getProfit(price, start, i - 1)
								+ getProfit(price, j + 1, end);
						profit.set(Math.max(profit.get(), currentProfit));
					}
				}));

		return profit.get();
	}

}
